package com.company;

import java.sql.Time;
import java.util.ArrayList;

public class Statistic {

    public static float getWorkingCoeff( CafeteriaBot bot) {
        return (float)bot.getTimeWork()/(float)bot.getTimeOn();
    }

    public static float getAverageTimeInBuff(ArrayList<CafeteriaBot> bots, int numberOfSource)
    {
        ArrayList<Order> reassembledSource = new ArrayList<>();
        for (int i = 0; i < bots.size(); i++) {
           ArrayList<Order> curBotList = bots.get(i).getFinishedOrders();
            for (int j = 0; j < curBotList.size(); j++) {
                if (curBotList.get(j).getSourceNumber()==numberOfSource)
                {
                    reassembledSource.add(curBotList.get(j));
                }
            }
        }
        long average = 0;
        for (int i = 0; i < reassembledSource.size(); i++) {
            {
                average+=reassembledSource.get(i).getEndTimeBuffer()-reassembledSource.get(i).getStartTimeBuffer();
            }
        }
        if (reassembledSource.size()>0)
        {
            average = (average / reassembledSource.size());
            return (float) average;
        }
        return 0;
    }
    public static float getAverageTimeInDevice(ArrayList<CafeteriaBot> bots, int numberOfSource)
    {
        ArrayList<Order> reassembledSource = new ArrayList<>();
        for (int i = 0; i < bots.size(); i++) {
            ArrayList<Order> curBotList = bots.get(i).getFinishedOrders();
            for (int j = 0; j < curBotList.size(); j++) {
                if (curBotList.get(j).getSourceNumber()==numberOfSource)
                {
                    reassembledSource.add(curBotList.get(j));
                }
            }
        }
        long average = 0;
        for (int i = 0; i < reassembledSource.size(); i++) {
            {
                average+=reassembledSource.get(i).getEndTimeBot()-reassembledSource.get(i).getEndTimeBuffer();
            }
        }
        if (reassembledSource.size()>0)
        {
            average = (average / reassembledSource.size());
            return (float) average;
        }
        return 0;
    }
    public static float getAverageTimeInProg(ArrayList<CafeteriaBot> bots, int numberOfSource)
    {
        ArrayList<Order> reassembledSource = new ArrayList<>();
        for (int i = 0; i < bots.size(); i++) {
            ArrayList<Order> curBotList = bots.get(i).getFinishedOrders();
            for (int j = 0; j < curBotList.size(); j++) {
                if (curBotList.get(j).getSourceNumber()==numberOfSource)
                {
                    reassembledSource.add(curBotList.get(j));
                }
            }
        }
        long average = 0;
        for (int i = 0; i < reassembledSource.size(); i++) {
            {
                average+=reassembledSource.get(i).getEndTimeBot()-reassembledSource.get(i).getStartTimeBuffer();
            }
        }
        if (reassembledSource.size()>0)
        {
            average = (average / reassembledSource.size());
            return (float) average;
        }
        return 0;
    }
    public static float getDeniedPercent(OrderSource source)
    {
        return ((float)source.getDenied() / (float)source.getTotal());
    }

    public static float getWaitingDispersion(ArrayList<CafeteriaBot> bots, OrderSource source)
    {
        float average = getAverageTimeInBuff(bots, source.getNumber());
        ArrayList<Order> reassembledSource = new ArrayList<>();
        for (int i = 0; i < bots.size(); i++) {
            ArrayList<Order> curBotList = bots.get(i).getFinishedOrders();
            for (int j = 0; j < curBotList.size(); j++) {
                if (curBotList.get(j).getSourceNumber()==source.getNumber())
                {
                    reassembledSource.add(curBotList.get(j));
                }
            }
        }
        float sum = 0;
        for (int i = 0; i < reassembledSource.size(); i++)
        {
            sum += Math.pow(((reassembledSource.get(i).getEndTimeBuffer()-reassembledSource.get(i).getStartTimeBuffer()) - average), 2);
        }
        return sum/source.getTotal();
    }

    public static float getDeviceDispersion(ArrayList<CafeteriaBot> bots, OrderSource source)
    {
        float average = getAverageTimeInDevice(bots, source.getNumber());
        ArrayList<Order> reassembledSource = new ArrayList<>();
        for (int i = 0; i < bots.size(); i++) {
            ArrayList<Order> curBotList = bots.get(i).getFinishedOrders();
            for (int j = 0; j < curBotList.size(); j++) {
                if (curBotList.get(j).getSourceNumber()==source.getNumber())
                {
                    reassembledSource.add(curBotList.get(j));
                }
            }
        }
        float sum = 0;
        for (int i = 0; i < reassembledSource.size(); i++)
        {
            sum += Math.pow(((reassembledSource.get(i).getEndTimeBot()-reassembledSource.get(i).getEndTimeBuffer()) - average), 2);
        }
        return sum/source.getTotal();
    }
}
