package com.company;

import java.util.ArrayList;

public class BufferManager {
     Buffer managedBuffer;

    BufferManager(Buffer ourBuff)
    {
        managedBuffer= ourBuff;
    }

    public void writeInBuffer (Order ourOrder, Buffer buff, OrderSource source) {
        synchronized (buff.getOrderList())
        {
            if (buff.getSizeOfBuffer() == buff.getCurrInBuffer()) {
                buff.denyOrder(ourOrder, source);
            } else {
                if (buff.get(buff.getPointer()) != null) {

                    while (buff.get(buff.getPointer()) != null) {
                        buff.movePointer();
                    }
                }
                ourOrder.setStartTimeBuffer(System.currentTimeMillis());
                buff.remove(buff.getPointer());
                buff.add(ourOrder);
                buff.movePointer();
            }
        }
    }
}
