package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    /*

19. ИБ И31 П32 Д1031 Д10О1 Д2П1 Д2Б5 ОР1 ОД1

ИБ - Бесконечный источник
И31 - Пуассоновский
П32 - Равномерный закон распределения времени обслуживания
Д1031 - Закон записи в буфер по кольцу
Д10О1 - Отказ по указателю
Д2П1 - Выбор прибора по номеру (чем меньше - тем приоритетней)
Д2Б5 - Выбор по номеру источника, заявки в пакете
ОР1 - Сводная таблица результатов
ОД1 - Календарь событий
     */

    static int totalReq;
    static int totalDev;
    static int totalSources;
    static boolean isDonGen = false;
    static boolean isStep = false;

    public static void main(String[] args) {
        final Object stepNotifier = new Object();
        final int BUFFER_SIZE = 100;
        final int SOURCES_COUNT = 6;
        final int DEVICES_COUNT = 3;
        final int REQUEST_COUNT = 100;
        final float LAMBDA[] = {1,1,1,1,1,1};

        System.out.print("Do you want to use step mode? (Y/N)\n");
        Scanner sc = new Scanner(System.in);
        String in = sc.next();
        if (in.equalsIgnoreCase("Y")) {
            isStep=true;
        }
        Buffer buff = new Buffer(BUFFER_SIZE);
        BufferManager buffMan = new BufferManager(buff);

        ArrayList<CafeteriaBot> bots = new ArrayList<>(DEVICES_COUNT);
        ArrayList<Thread> botThreads = new ArrayList<>(DEVICES_COUNT);
        for (int i = 0; i < DEVICES_COUNT; i++) {
            CafeteriaBot device = new CafeteriaBot(stepNotifier);
            bots.add(device);
            botThreads.add(new Thread(device));
        }

        DeviceManager devMan = new DeviceManager(buffMan.managedBuffer, bots,stepNotifier);

        ArrayList<Thread> sources = new ArrayList<>(SOURCES_COUNT);
        ArrayList<OrderSource> source = new ArrayList<>(SOURCES_COUNT);
        for (int i = 0; i < SOURCES_COUNT; i++) {
            source.add(new OrderSource(LAMBDA[i], buffMan,stepNotifier));
            sources.add(new Thread(source.get(i)));
        }
        Thread dmThread = new Thread(devMan);

        //Start!
        int step = 0;
        long startTime = System.currentTimeMillis();
        sources.forEach(Thread::start);
        dmThread.start();
        while (sources.get(0).isInterrupted() == false) {
            if (Main.isStep==true) {
                System.out.print(++step+" step, current system time is "+ (System.currentTimeMillis()-startTime) +"\n");
                System.out.print("\nCurrently in Buffer: "+ buffMan.managedBuffer.getCurrInBuffer()+"\nPointer position = "+buffMan.managedBuffer.getPointer()+'\n');
                synchronized (devMan.devices) {
                    for (int i = 0; i < DEVICES_COUNT; i++) {
                        if ((devMan.devices.get(i).getPrep().size()>0)&&(!(devMan.devices.get(i).isFree())))
                            System.out.print("Device number " + devMan.devices.get(i).getDevNumb() + " is processing request from " +
                                    devMan.devices.get(i).getPrep().get(0).getSourceNumber() + " source\n");
                        else
                            System.out.print("Device number " + devMan.devices.get(i).getDevNumb() + " is free\n");
                    }
                }
                synchronized (stepNotifier) {
                    stepNotifier.notify();
                    sc.nextLine();
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (totalReq >= REQUEST_COUNT) {
                sources.forEach(Thread::interrupt);
                isDonGen=true;
            }
        }
        System.out.print("\nREQUEST GENERATION IS OVER, SOURCES GENERATE MAX ONE LAST REQUEST, DEVICES FINISHING THEIR WORK\n\n");
        while (dmThread.isAlive()==true)
        {

            if (Main.isStep==true) {
                System.out.print(++step+" step, current system time is "+ (System.currentTimeMillis()-startTime) +"\n");
                System.out.print("\nCurrently in Buffer: "+ buffMan.managedBuffer.getCurrInBuffer()+'\n');
                for (int i = 0; i < DEVICES_COUNT; i++) {
                    if ((devMan.devices.get(i).getPrep().size()>0)&&(!(devMan.devices.get(i).isFree())))
                        System.out.print("Device number " + devMan.devices.get(i).getDevNumb() + " is processing request from " +
                                devMan.devices.get(i).getPrep().get(0).getSourceNumber() + " source\n");
                    else
                        System.out.print("Device number " + devMan.devices.get(i).getDevNumb() + " is free\n");
                }
                synchronized (stepNotifier) {
                    stepNotifier.notify();
                    sc.nextLine();
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            else {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        devMan.devices.forEach(CafeteriaBot::OffDevice);
       System.out.print("\nBot Amount: "+ DEVICES_COUNT+"\nBuffer Capacity:"+BUFFER_SIZE+"\nSources amount:"+SOURCES_COUNT
                +"\nRequests expected: "+REQUEST_COUNT +"\nRequests Generated: "+totalReq+"\n\nSee More Info? (Y/N)");
        Scanner input = new Scanner(System.in);
        String str = input.next();
        if (str.equalsIgnoreCase("Y")) {
            System.out.print("\n What info do you want to know? (Enter number)");
            System.out.print("\n1. Sources, Their intensity, amount of generated Requests, amount of denied Request and % of denial");
            System.out.print("\n2. Average Time in Buffer and Device, and average end time per source");
            System.out.print("\n3. Bots and their working coefficient");
            System.out.print("\n4. Dispersion");
            System.out.print("\n5. Exit Program\n");

            while (str!="5"){
                str = input.next();
            switch (str) {
                case "1":
                    System.out.print("\nColumns: 1 - Intensity, 2 - Total Request, 3 - Denied Requests, 4 - Denial Percent\n");
                    for (int i = 0; i < SOURCES_COUNT; i++) {
                        System.out.print("\n"+ source.get(i).getNumber() + ": "+ source.get(i).getSourceInterval()+
                                "    "+ source.get(i).getTotal()+ "    " + source.get(i).getDenied()+ "    "+
                                Statistic.getDeniedPercent(source.get(i)));
                    }
                    System.out.print("\n");
                    break;
                case "2":
                    System.out.print("\nColumns: 1 - Average time in buffer, 2 - Average time in device, 3 - Average time in system\n");
                    for (int i = 0; i < SOURCES_COUNT; i++) {
                    float averageOne = Statistic.getAverageTimeInBuff(devMan.devices,source.get(i).getNumber());
                    float averageTwo = Statistic.getAverageTimeInDevice(devMan.devices,source.get(i).getNumber());
                    float averageThree = Statistic.getAverageTimeInProg(devMan.devices,source.get(i).getNumber());
                        System.out.print("\n"+ source.get(i).getNumber() + ":     " + averageOne +
                                "     " + averageTwo + "     "+ averageThree );
                    }
                    System.out.print("\n");
                    break;
                case "3":
                    System.out.print("\nColumns: 1 - Working coeff of device\n");
                    for (int i = 0; i < DEVICES_COUNT; i++) {
                        float coeff = Statistic.getWorkingCoeff(devMan.devices.get(i));
                        System.out.print("\n"+ devMan.devices.get(i).getDevNumb()+ ": " + coeff );
                    }
                    System.out.print("\n");
                    break;
                case "4":
                    System.out.print("\nColumns: 1 - Dispersion of time in buffer, 2 -  Dispersion of time in device\n");
                    for (int i = 0; i < SOURCES_COUNT; i++) {
                        float dOne = Statistic.getWaitingDispersion(devMan.devices,source.get(i));
                        float dTwo = Statistic.getDeviceDispersion(devMan.devices,source.get(i));
                        System.out.print("\n"+ source.get(i).getNumber() + ":     " + dOne +
                                "     " + dTwo );
                    }
                    System.out.print("\n");
                    break;
                case "5":
                    return;
                default:
                    System.out.print("\nNot an available number, please choose once more");
                    break;
            }
            }
        } else return;
    }

}

