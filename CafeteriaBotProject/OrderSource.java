package com.company;


import static java.lang.Thread.sleep;

public class OrderSource implements Runnable{
    private BufferManager buffMan;
    private float sourceInterval;
    private int number;
    private int totalCount=0;
    private int refusedCount=0;
    private Order lastOrder;
    private final Object stepNotify;

    public OrderSource(float lambda, BufferManager buffman, Object notify)
    {
        Main.totalSources++;
        number=Main.totalSources;
        buffMan=buffman;
        setSourceInterval(lambda);
        lastOrder=null;
        stepNotify = notify;
    }

    public float getSourceInterval() {
        return sourceInterval;
    }
    public void setSourceInterval(float lambda) {
        float random = (float)Math.random();//(float)0.7;
        this.sourceInterval =  (float)((-1/lambda)*Math.log(random));
    }
    public int getNumber() {
        return number;
    }
    public int getTotal() {
        return totalCount;
    }
    public int getDenied() {
        return refusedCount;
    }
    public void addDenied() {
        refusedCount++;
    }
    public Order puassonSource( )
    {
        Order genOrder = new Order();
        if (lastOrder==null)
             genOrder.setStartTime(System.currentTimeMillis()+(long)this.sourceInterval);
        else {
            long prevTime;
            prevTime = System.currentTimeMillis() - lastOrder.getStartTime().getTime();
            genOrder.setStartTime((System.currentTimeMillis() + (long) this.sourceInterval + prevTime));
        }
        genOrder.setSourceNumber(number);
        this.lastOrder = genOrder;
        return genOrder;
    }

    @Override
    public void run()
    {
        while (!Thread.currentThread().isInterrupted()) {
            if (Main.isStep==true) {
                synchronized (stepNotify) {
                   try {
                        stepNotify.wait();
                    } catch (InterruptedException ignored) {
                        break;
                   }
                }
            }
            Order newOrder = puassonSource();
            lastOrder=newOrder;
            buffMan.writeInBuffer(newOrder, buffMan.managedBuffer,this);
            System.out.print("Source Number "+ this.number+ " generated Order "+newOrder.getNumber()+'\n');
            totalCount++;
            try {
                sleep((long) sourceInterval);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                break;
            }
        }

    }

}


