package com.company;

import java.util.ArrayList;
import java.util.Random;

import static java.lang.Thread.sleep;

public class CafeteriaBot implements Runnable {
    private int devNumb;
    private boolean hasOrder;
    private final Object stepNotify;
    private ArrayList<Order> ordersToPrep;
    private ArrayList<Order> finishedOrders;
    private long timeOn;
    private long timeWork;

    public CafeteriaBot(Object notifier)
    {
        hasOrder=false;
        ordersToPrep = new ArrayList<Order>();
        Main.totalDev++;
        devNumb=Main.totalDev;
        finishedOrders=new ArrayList<Order>();
        timeOn = 0;
        timeWork=0;
        timeOn=System.currentTimeMillis();
        stepNotify = notifier;
    }
    public int getDevNumb() {return devNumb;}
    public long getTimeOn(){
        return timeOn;
    }
    public long getTimeWork() {return timeWork;}
    public boolean isFree()
    {
        if (hasOrder==false)
            return true;
        else
        return false;
    }
    public ArrayList<Order> getPrep()
    { return ordersToPrep;}
    public ArrayList<Order> getFinishedOrders() {
        return finishedOrders;
    }
    public void setOrder( ArrayList<Order> newOrder)
    {
        ordersToPrep=newOrder;
        hasOrder=true;
    }
    public void OffDevice()
    {
        this.timeOn=System.currentTimeMillis()-timeOn;
    }

    @Override
    public void run() {
        long addtimeWork = System.currentTimeMillis();
            if (ordersToPrep.isEmpty());
            {
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            while ((!ordersToPrep.isEmpty())) {
                if (Main.isStep==true) {
                    synchronized (stepNotify) {
                        try {
                            stepNotify.wait();
                        } catch (InterruptedException ignored) {
                            break;
                        }
                    }
                }
                Order curOrder = ordersToPrep.get(0);
                curOrder.setEndTimeBuffer(System.currentTimeMillis());
                Random random = new Random();
                long maxTime = curOrder.getAmountOfFood() * 10;
                long minTime = curOrder.getAmountOfFood();
                long newTime = (long) ((maxTime-minTime)*Math.random())+minTime;
                try {
                    sleep( newTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                curOrder.setEndTimeBot(System.currentTimeMillis());
                System.out.print("Done With Order " + curOrder.getNumber() + " From Source " + curOrder.getSourceNumber() + ", had " + curOrder.getAmountOfFood()
                        + " items on it, spend " + (curOrder.getEndTimeBot() - curOrder.getEndTimeBuffer()) + "millis in device " + devNumb + '\n'
                        + "Spent " + (curOrder.getEndTimeBuffer() - curOrder.getStartTimeBuffer() + " millis in buffer\n"));

                finishedOrders.add(curOrder);
                if (!ordersToPrep.isEmpty()) {
                    ordersToPrep.remove(0);
                }
            }
            hasOrder = false;
            addtimeWork = System.currentTimeMillis()-addtimeWork;
            timeWork += addtimeWork;
            return;
        }

}
