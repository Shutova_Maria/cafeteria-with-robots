package com.company;
import java.util.ArrayList;

public class Buffer {
    static int totalBuffAmount;
    private int pointer=0;
    private int number;
    private int sizeOfBuffer;
    private int currentlyInBuffer=0;
    private int deniedAmount=0;
    private ArrayList<Order> waitingOrderList;

    public Buffer( int size)
    {
        this.sizeOfBuffer=size;
        totalBuffAmount++;
        this.number=totalBuffAmount;
        this.waitingOrderList = new ArrayList<Order>(sizeOfBuffer);
        for( int i = 0; i < this.sizeOfBuffer; i++) {
        waitingOrderList.add(null);
        }

    }

    public int getSizeOfBuffer() {
        return sizeOfBuffer;
    }

    public void countCurr() {
        int curr = 0;
        for (int i=0;i<this.getSizeOfBuffer();i++)
        {
            if (this.get(i)!=null)
            {
                curr++;
            }
            this.currentlyInBuffer=curr;
        }
    }
    public int getDeniedAmount() {
        return deniedAmount;
    }
    public ArrayList<Order> getOrderList() {
        return waitingOrderList;
    }
    public void setDeniedAmount(int deniedAmount) {
        this.deniedAmount = deniedAmount;
    }
    public void setOrderList(ArrayList<Order> orderList) {
        this.waitingOrderList = orderList;
    }
    public void setSizeOfBuffer(int sizeOfBuffer) {
        this.sizeOfBuffer = sizeOfBuffer;
    }
    public double getNumber() {
        return number;
    }
    public int getPointer() {
        return pointer;
    }
    public void setNumber(int newNumber) {
        this.number = newNumber;
    }
    public Order get( int index) {
        return this.waitingOrderList.get(index);
    }
    public double getCurrInBuffer() {
        return currentlyInBuffer;
    }
    public void setCurrInBuffer(int newNumber) {
        this.currentlyInBuffer = newNumber;
    }

    public void add (Order newOrder)
    {
        waitingOrderList.set(pointer,newOrder);
        currentlyInBuffer++;
    }
    public void remove (int pointer)
    {
        if (this.waitingOrderList.get(pointer)!=null) {
            currentlyInBuffer--;
        }
        this.waitingOrderList.set(pointer,null);
    }
    public void movePointer ()
    {
        pointer++;
        if (pointer>=sizeOfBuffer)
        {
            pointer=0;
        }
    }

    public void denyOrder (Order ourOrder, OrderSource source)
    {
        Order deletedOrder = this.waitingOrderList.get(this.pointer);
        System.out.print("Request number "+ deletedOrder.getNumber() + " was denied"+'\n');
        this.waitingOrderList.remove(this.pointer);
        ourOrder.setStartTimeBuffer(System.currentTimeMillis());
        this.waitingOrderList.add(this.pointer, ourOrder);
        movePointer();
        source.addDenied();
    }

}
