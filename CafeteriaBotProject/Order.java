package com.company;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Random;


public class Order {
    private ArrayList<String> listOfFood;
    private int amountOfFood;
    private int number;
    private  int numberOfSource;
    private Time startTime;
    private Time startTimeBuffer;
    private Time endTimeBuffer;
    private Time endTimeBot;
    public Order()
    {
        this.listOfFood = new ArrayList<String>();
        Main.totalReq++;
        this.number = Main.totalReq;
        this.startTime = new Time(0);
        this.endTimeBuffer= new Time(0);
        this.endTimeBot = new Time(0);
        this.startTimeBuffer = new Time(0);
        this.generateRandomOrder(this);

    }
    public ArrayList<String> getListOfFood() {
        return listOfFood;
    }
    public void setListOfFood(ArrayList<String> listOfFood) {
        this.listOfFood = listOfFood;
    }
    public int getAmountOfFood() {
        return amountOfFood;
    }
    public void setAmountOfFood(int amountOfFood) {
        this.amountOfFood = amountOfFood;
    }
    public int getNumber() {
        return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }
    public int getSourceNumber() {
        return numberOfSource;
    }
    public Time getStartTime() {
        return startTime;
    }
    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }
    public void setStartTime(long startTime) {
        this.startTime = new Time(startTime);
    }
    public long getStartTimeBuffer() {
        return startTimeBuffer.getTime();
    }
    public void setSourceNumber(int numb) {this.numberOfSource=numb;}

    public void setStartTimeBuffer(long startTime) {
        this.startTimeBuffer = new Time(startTime);
    }

    public long getEndTimeBuffer() {
        return endTimeBuffer.getTime();
    }
    public void setEndTimeBuffer(long endTimeBuffer) {
        this.endTimeBuffer = new Time(endTimeBuffer);
    }
    public long getEndTimeBot() {
        return endTimeBot.getTime();
    }
    public void setEndTimeBot(long endTimeBot) {
        this.endTimeBot = new Time(endTimeBot);
    }

    public void generateRandomOrder( Order ordToRand)
    {
        Random rand = new Random();
        int k = ordToRand.amountOfFood = rand.nextInt(6)+1;
        while (k>0)
        {
           int posGen = rand.nextInt(7);
            switch (posGen) {
                case  (1):
                    ordToRand.listOfFood.add("Котлеты");
                    break;
                case  (2):
                    ordToRand.listOfFood.add("Пюре");
                    break;
                case  (3):
                    ordToRand.listOfFood.add("Хлеб");
                    break;
                case (4):
                    ordToRand.listOfFood.add("Макароны");
                    break;
                case (5):
                    ordToRand.listOfFood.add("Каша");
                    break;
                case  (6):
                    ordToRand.listOfFood.add("Пирожное");
                    break;
                default:
                    ordToRand.listOfFood.add("Вода");
                    break;
            }
            k-=1;
        }
    }
}
