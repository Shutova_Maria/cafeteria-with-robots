package com.company;

import java.util.ArrayList;
import java.util.Comparator;

public class DeviceManager implements Runnable{
    Buffer buffer;
    ArrayList<CafeteriaBot> devices;
    private final Object stepNotify;


    DeviceManager(Buffer newBuffer, ArrayList<CafeteriaBot> newDevices, Object notify)
    {
        buffer=newBuffer;
        devices=newDevices;
        stepNotify = notify;
    }
    public boolean isBufferEmpty(Buffer ourBuffer)
    {
        if (ourBuffer.getSizeOfBuffer()==0)
        return true;
        return false;
    }
    public ArrayList<Order> formOrderPackage(Buffer buff, int sourceNumber)
    {
      // synchronized (buff)
        {
            ArrayList<Order> pack = new ArrayList<>();
            int i = 0;
            while ((i < buff.getSizeOfBuffer()) && (findMinSource() == sourceNumber)) {
                if ((buff.getOrderList().get(i)!=null)&&(buff.getOrderList().get(i).getSourceNumber() == sourceNumber)) {
                    pack.add(buff.getOrderList().get(i));
                    buff.remove(i);
                    //i--;
                }
                i++;
            }
            return pack;
        }
    }
    public CafeteriaBot chooseDevice()
    {
      //  synchronized (devices)
        {
            devices.sort(Comparator.comparing(CafeteriaBot::getDevNumb));
            int minNum = this.devices.get(devices.size() - 1).getDevNumb();
            int minPos = devices.size() - 1;
            for (int i = 0; i < devices.size(); i++) {
                if (((this.devices.get(i).isFree())) && (this.devices.get(i).getDevNumb() < minNum)) {
                    minNum = this.devices.get(i).getDevNumb();
                    minPos = i;
                }
            }
            return devices.get(minPos);
        }
    }

    public int findMinSource()
    {
       // synchronized (buffer)
           {
            int minNum = Main.totalSources;
            for (int i = 0; (i < buffer.getSizeOfBuffer()); i++) {
                if ((buffer.get(i)!=null)&&(buffer.get(i).getSourceNumber() < minNum)) {
                    minNum = buffer.get(i).getSourceNumber();
                }
            }
            return minNum;
       }
    }

    @Override
    public void run() {
      //  synchronized (buffer)
        {
            while (!Thread.currentThread().isInterrupted()) {
                this.buffer.countCurr();
                if (this.buffer.getCurrInBuffer() != 0) {
                    int minNum = findMinSource();
                    CafeteriaBot chosenDevice = chooseDevice();
                    if (chosenDevice.isFree() == false) {
                            while (chosenDevice.isFree()==false) {
                                chosenDevice = chooseDevice();
                            }
                    }
                    ArrayList<Order> ourOrders = formOrderPackage(buffer, minNum);
                    chosenDevice.setOrder(ourOrders);
                    if (Main.isStep==true) {
                        synchronized (stepNotify) {
                            try {
                                stepNotify.wait();
                            } catch (InterruptedException ignored) {
                                break;
                            }
                        }
                    }
                    Thread newThread = new Thread(chosenDevice);
                    newThread.start();
                }
                else
                {
                    if (Main.isDonGen)
                    {

                        boolean allFree=true;
                        for(int i=0;i<this.devices.size();i++)
                        {
                            if (devices.get(i).isFree()==false)
                                allFree=false;
                        }
                        if (allFree==true)
                        {
                            return;
                        }
                    }
                    {
                        try {
                            Thread.sleep(0);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        }
    }

}
